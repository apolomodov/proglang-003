(* Coursera Programming Languages, Homework 3 *)

exception NoAnswer

datatype pattern = Wildcard
		 | Variable of string
		 | UnitP
		 | ConstP of int
		 | TupleP of pattern list
		 | ConstructorP of string * pattern

datatype valu = Const of int
		  | Unit
		  | Tuple of valu list
		  | Constructor of string * valu

fun g f1 f2 p =
	let 
	val r = g f1 f2 
	in
	case p of
		Wildcard		  => f1 ()
	  | Variable x		=> f2 x
	  | TupleP ps		 => List.foldl (fn (p,i) => (r p) + i) 0 ps
	  | ConstructorP(_,p) => r p
	  | _				 => 0
	end

(**** for the challenge problem only ****)

datatype typ = Anything
		 | UnitT
		 | IntT
		 | TupleT of typ list
		 | Datatype of string

(**** you can put all your code here ****)

(* 1. fun only capitals *)
fun only_capitals(ls: string list) =
	List.filter (fn str => Char.isUpper(String.sub(str, 0))) ls

(* 1. test *)
val string_list_1 = ["Hello", "dear", "friend"]
val string_list_2 = ["This", "is", "test", "for", "checking", "first", "second", "third", "Problems"]
val only_capitals_1 = only_capitals(string_list_1)
val only_capitals_2 = only_capitals(string_list_2)


(* 2. fun longest_string1 *)
fun longest_string1(ls: string list) =
	foldl (fn (x, y) => if String.size(x) > String.size(y) then x else y) "" ls

(* 2. test *)
val longest_string_1 = longest_string1(string_list_1)
val longest_string_2 = longest_string1(string_list_2)


(* 3. fun longest_string2 *)
fun longest_string2(ls: string list) =
	foldl (fn (x, y) => if String.size(x) >= String.size(y) then x else y) "" ls

(* 3. test *)
val longest_string2_1 = longest_string2(string_list_1)
val longest_string2_2 = longest_string2(string_list_2)


(* 4. fun longest_string_helper, fun longest_string3, longest_string4 *)
fun longest_string_helper predicate ls = 
	foldl (fn (x, y) =>  if predicate(String.size(x), String.size(y)) then x else y) "" ls

val longest_string3 = longest_string_helper (fn(x, y) => x > y)

val longest_string4 = longest_string_helper (fn(x, y) => x >= y)

(* 4. test *)

val longest_string3_1 = longest_string3 string_list_1
val longest_string3_2 = longest_string3 string_list_2

val longest_string4_1 = longest_string4 string_list_1
val longest_string4_2 = longest_string4 string_list_2

(* 5. fun longest_capitalized *)
fun longest_capitalized(ls: string list) =
	(longest_string1 o only_capitals) ls

(* 5. test *)
val longest_capitalized_test_string = ["This", "is", "test", "for", "Checking", "first", "second", "third", "Problems"]
val longest_capitalized1 = longest_capitalized(string_list_1)
val longest_capitalized2 = longest_capitalized(string_list_2)
val longest_capitalized3 = longest_capitalized(longest_capitalized_test_string)


(* 6. fun rev_string *)
fun rev_string(str: string) =
	(implode o rev o explode) str

(* 6. test *)
val rev_test_string1 = "Hello"
val rev_test_string_res = rev_string(rev_test_string1)


(* 7. fun first_answer *)
fun first_answer predicate ls = 
	case ls of
		[] => raise NoAnswer
		| x::xs => case predicate x of
			NONE =>  first_answer predicate xs
			| SOME v => v


(* 8. fun all_answers *)
fun all_answers predicate ls = 
	let
		fun aux(predicate, ls, accum) =
			case ls of
				[] => accum
				| x::xs => case predicate x of
					NONE => NONE
					| SOME v => case accum of
						NONE => NONE
						| SOME acc => aux(predicate, xs, SOME (v @ acc))
	in
		aux(predicate, ls, SOME [])
	end


(* 9.a fun count_wildcards*)
fun count_wildcards pattern =
	g (fn x => 1) (fn y => 0) pattern

(* 9.b fun count_wild_and_variable_lengths *)
fun count_wild_and_variable_lengths pattern =
	g (fn x => 0) (fn y => String.size y) pattern + count_wildcards pattern

(* 9.c fun count_some_var *) 
fun count_some_var(str: string, pattern) =
	g (fn _ => 0) (fn y => if y = str then 1 else 0) pattern

(* 10 fun check_pat *)
fun check_pat p =
	let fun get_str(p) =
		case p of
			Wildcard		  => []
		  | Variable x		=> [x]
		  | TupleP ps		 => List.foldl (fn (p,s) => s @ get_str(p)) [] ps
		  | ConstructorP(_,p) => get_str(p)
		  | _				 => []
	fun check_for_dups(sl) =
		case sl of
			 [] => true
		   | h::t => if List.exists (fn x => h = x) t then false
					 else check_for_dups(t)
	in
		check_for_dups(get_str(p))
	end

   
(* 11. fun match *)
fun match (v,p) =
	case (v,p) of
		 (_,Wildcard) => SOME []
	   | (Unit,UnitP) => SOME []
	   | (v,Variable s) => SOME [(s,v)]
	   | (Const x,ConstP y) => if x = y then SOME [] else NONE
	   | (Constructor(s1,v2),ConstructorP(s2,p2)) => if (s1 = s2) then match(v2,p2) else NONE
	   | (Tuple vl,TupleP pl) => (all_answers (match) (ListPair.zipEq(vl,pl))
								  handle ListPair.UnequalLengths => NONE)
	   | _ => NONE


(* 12. val first_match = fn : valu -> pattern list -> (string * valu) list option*)
fun first_match v plist =
	let fun curry f x y = f(x,y)
	in
		SOME (first_answer (curry match v) plist)
		handle NoAnswer => NONE
	end



