(* 1. is_older function *)
fun is_older(date1: int*int*int, date2: int*int*int) =
	if(#1 date1 < #1 date2)
	then true
	else if(#1 date1 > #1 date2)
	then false
	else if(#2 date1 < #2 date2)
	then true
	else if(#2 date1 > #2 date2)
	then false
	else if(#3 date1 < #3 date2)
	then true
	else false

(* true *)
(* val is_older1 = is_older((2013, 2, 3), (2014, 2, 3)) *)
(* val is_older2 = is_older((2014, 1, 3), (2014, 2, 3)) *)
(* val is_older3 = is_older((2014, 2, 2), (2014, 2, 3)) *)


(* false *)
(* val is_older4 = is_older((2014, 2, 3), (2014, 2, 3)) *)
(* val is_older5 = is_older((2015, 1, 1), (2014, 2, 3)) *)
(* val is_older6 = is_older((2014, 3, 1), (2014, 2, 3)) *)
(* val is_older7 = is_older((2014, 2, 5), (2014, 2, 3)) *)

(* 2. number_is_month *)
fun number_in_month(dates: (int*int*int) list, month: int) = 
	if null dates
	then 0
	else if(month = #2 (hd dates))
	then 1 + number_in_month(tl dates, month)
	else number_in_month(tl dates, month)


(* val dates = [(2014, 3, 2), (2013, 3, 1), (2013, 7, 2), (2014, 12, 1), (2014, 3, 14), (2014, 7, 15)] *)

(* val number1 = number_in_month(dates, 3) (* 3 *) *)
(* val number2 = number_in_month(dates, 2) (* 0 *) *)
(* val number3 = number_in_month(dates, 7) (* 2 *) *)
(* val number4 = number_in_month(dates, 12) (* 1 *) *)

(* 3. number_in_months *)
fun number_in_months(dates: (int*int*int) list, months: int list) = 
	if null months
	then 0
	else number_in_month(dates, hd months) + number_in_months(dates, tl months)

(* val number5 = number_in_months(dates, [1,2,3,4,12]) (* 4 *) *)

(* 4. dates_in_month *)
fun dates_in_month(dates: (int*int*int) list, month: int) = 
	if null dates
	then []
	else if(month = #2 (hd dates))
	then hd dates :: dates_in_month(tl dates, month)
	else dates_in_month(tl dates, month)

(* val dates_in_month1 = dates_in_month(dates, 3) (* [(2014,3,2),(2013,3,1),(2014,3,14)] *) *)
(* val dates_in_month2 = dates_in_month(dates, 1) (* [] *) *)
(* val dates_in_month3 = dates_in_month(dates, 12) (* [(2014,12,1)] *) *)


(* 5. dates_in_months *)
fun dates_in_months(dates: (int*int*int) list, months: int list) = 
	if null months
	then []
	else dates_in_month(dates, hd months) @ dates_in_months(dates, tl months)

(* val dates_in_month4 = dates_in_months(dates, [1,4]) (* [] *) *)
(* val dates_in_month5 = dates_in_months(dates, [3, 12]) (* [(2014,3,2),(2013,3,1),(2014,3,14),(2014,12,1)] *) *)
(* val dates_in_month6 = dates_in_months(dates, [7,8]) (* [(2013,7,2),(2014,7,15)] *) *)

(* 6. get_nth *)
fun get_nth(elements: string list, position: int) = 
	if position = 1
	then hd elements
	else get_nth(tl elements, position - 1)

(* val elements = ["Hello", "dear", "friend", "Nice", "to", "meet", "you"] *)
(* val get_nth1 = get_nth(elements, 1) (* "Hello" *) *)
(* val get_nth2 = get_nth(elements, 3) (* "friend" *) *)
(* val get_nth3 = get_nth(elements, 121) *) (*  raise uncaught exception Empty *)

(* 7. date_to_string *)
fun date_to_string(date: int*int*int) = 
	let
		val months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
	in
		get_nth(months, #2 date) ^ " " ^ Int.toString(#3 date) ^ ", " ^ Int.toString(#1 date)
	end

(* val date_to_string1 = date_to_string((1986, 1, 3)) (* January 3, 1986 *) *)
(* val date_to_string2 = date_to_string((2014, 7, 14)) (* July 14, 2014 *) *)
(* val date_to_string3 = date_to_string((1999, 4, 7)) (* April 7, 1999 *) *)

(* 8. number before reaching sum *)
fun number_before_reaching_sum(sum: int, container: int list) = 
	let
		fun number_before(thesum: int, thecontainer: int list, thecurrent: int, step: int) = 
			if thecurrent < thesum
			then number_before(thesum, tl thecontainer, thecurrent + (hd thecontainer), step + 1)
			else step - 1
	in
		number_before(sum, container, 0, 0)
	end

(* val numbers = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55] *)
(* val number_before_reaching_sum1 = number_before_reaching_sum(10, numbers) (* 4 *) *)
(* val number_before_reaching_sum2 = number_before_reaching_sum(20, numbers) (* 5 *) *)
(* val number_before_reaching_sum3 = number_before_reaching_sum(60, numbers) (* 8 *) *)
(* val number_before_reaching_sum4 = number_before_reaching_sum(2, numbers) (* 1 *) *)

(* 9. what_month *)
fun what_month(day: int) = 
	let
		val number_of_day_in_months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
	in
		number_before_reaching_sum(day, number_of_day_in_months) + 1
	end


(* val what_month1 = what_month(30) (* 1 *) *)
(* val what_month2 = what_month(31) (* 1 *) *)
(* val what_month3 = what_month(32) (* 2 *) *)
(* val what_month4 = what_month(60) (* 3 *) *)
(* val what_month5 = what_month(364) (* 12 *) *)


(* 10. month_range *)
fun month_range(day1: int, day2: int) = 
	if day1 > day2
	then []
	else what_month(day1) :: month_range(day1 + 1, day2)

(* val month_range1 = month_range(27, 33) (* [1,1,1,1,1,2,2] *) *)
(* val month_range2 = month_range(55, 60) (* [2,2,2,2,2,3] *) *)

(* 11. oldest *)
fun oldest(dates: (int*int*int) list) =
	if null dates
	then NONE
	else let 
			fun oldest_nonempty(dates: (int*int*int) list) = 
				if null (tl dates)
				then hd dates
				else 
					let
						val older = is_older(hd dates,  oldest_nonempty(tl dates))
					in
						if older
						then hd dates
						else oldest_nonempty(tl dates)
					end
		in
			SOME(oldest_nonempty(dates))
		end


(* val oldtest1 = oldest([]) (* NONE *) *)
(* val oldtest2 = oldest(dates) (* SOME (2013,3,1) *) *)