(* 1. is_older function *)
(* true *)
(* val is_older1 = is_older((2013, 2, 3), (2014, 2, 3)) *)
(* val is_older2 = is_older((2014, 1, 3), (2014, 2, 3)) *)
(* val is_older3 = is_older((2014, 2, 2), (2014, 2, 3)) *)


(* false *)
(* val is_older4 = is_older((2014, 2, 3), (2014, 2, 3)) *)
(* val is_older5 = is_older((2015, 1, 1), (2014, 2, 3)) *)
(* val is_older6 = is_older((2014, 3, 1), (2014, 2, 3)) *)
(* val is_older7 = is_older((2014, 2, 5), (2014, 2, 3)) *)

(* 2. number_is_month *)
(* val dates = [(2014, 3, 2), (2013, 3, 1), (2013, 7, 2), (2014, 12, 1), (2014, 3, 14), (2014, 7, 15)] *)

(* val number1 = number_in_month(dates, 3) (* 3 *) *)
(* val number2 = number_in_month(dates, 2) (* 0 *) *)
(* val number3 = number_in_month(dates, 7) (* 2 *) *)
(* val number4 = number_in_month(dates, 12) (* 1 *) *)

(* 3. number_in_months *)
(* val number5 = number_in_months(dates, [1,2,3,4,12]) (* 4 *) *)

(* 4. dates_in_month *)
(* val dates_in_month1 = dates_in_month(dates, 3) (* [(2014,3,2),(2013,3,1),(2014,3,14)] *) *)
(* val dates_in_month2 = dates_in_month(dates, 1) (* [] *) *)
(* val dates_in_month3 = dates_in_month(dates, 12) (* [(2014,12,1)] *) *)


(* 5. dates_in_months *)
(* val dates_in_month4 = dates_in_months(dates, [1,4]) (* [] *) *)
(* val dates_in_month5 = dates_in_months(dates, [3, 12]) (* [(2014,3,2),(2013,3,1),(2014,3,14),(2014,12,1)] *) *)
(* val dates_in_month6 = dates_in_months(dates, [7,8]) (* [(2013,7,2),(2014,7,15)] *) *)

(* 6. get_nth *)
(* val elements = ["Hello", "dear", "friend", "Nice", "to", "meet", "you"] *)
(* val get_nth1 = get_nth(elements, 1) (* "Hello" *) *)
(* val get_nth2 = get_nth(elements, 3) (* "friend" *) *)
(* val get_nth3 = get_nth(elements, 121) *) (*  raise uncaught exception Empty *)

(* 7. date_to_string *)
(* val date_to_string1 = date_to_string((1986, 1, 3)) (* January 3, 1986 *) *)
(* val date_to_string2 = date_to_string((2014, 7, 14)) (* July 14, 2014 *) *)
(* val date_to_string3 = date_to_string((1999, 4, 7)) (* April 7, 1999 *) *)

(* 8. number before reaching sum *)
(* val numbers = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55] *)
(* val number_before_reaching_sum1 = number_before_reaching_sum(10, numbers) (* 4 *) *)
(* val number_before_reaching_sum2 = number_before_reaching_sum(20, numbers) (* 5 *) *)
(* val number_before_reaching_sum3 = number_before_reaching_sum(60, numbers) (* 8 *) *)
(* val number_before_reaching_sum4 = number_before_reaching_sum(2, numbers) (* 1 *) *)

(* 9. what_month *)
(* val what_month1 = what_month(30) (* 1 *) *)
(* val what_month2 = what_month(31) (* 1 *) *)
(* val what_month3 = what_month(32) (* 2 *) *)
(* val what_month4 = what_month(60) (* 3 *) *)
(* val what_month5 = what_month(364) (* 12 *) *)


(* 10. month_range *)
(* val month_range1 = month_range(27, 33) (* [1,1,1,1,1,2,2] *) *)
(* val month_range2 = month_range(55, 60) (* [2,2,2,2,2,3] *) *)

(* 11. oldest *)
(* val oldtest1 = oldest([]) (* NONE *) *)
(* val oldtest2 = oldest(dates) (* SOME (2013,3,1) *) *)