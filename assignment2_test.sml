(* Dan Grossman, Coursera PL, HW2 Provided Code *)

(* if you use this function to compare two strings (returns true if the same
   string), then you avoid several of the functions in problem 1 having
   polymorphic types that may be confusing *)
fun same_string(s1 : string, s2 : string) =
    s1 = s2

(* put your solutions for problem 1 here *)

(* you may assume that Num is always used with values 2, 3, ..., 10
   though it will not really come up *)
datatype suit = Clubs | Diamonds | Hearts | Spades
datatype rank = Jack | Queen | King | Ace | Num of int 
type card = suit * rank

datatype color = Red | Black
datatype move = Discard of card | Draw 

exception IllegalMove

(* put your solutions for problem 2 here *)


(* 1 Substitution problems *)

(* 1a fun all_except_option *)
fun all_except_option(needle: string, haystack: string list) =
	let 
		fun aux(needle: string, remaining: string list, accum: string list, found) =
			case remaining of
				[] => if found then SOME(accum) else NONE
				| x::xs => if same_string(x, needle) then aux(needle, xs, accum, true) else aux(needle, xs, x :: accum, found)
	in
		aux(needle, haystack, [], false)
	end

(* 1a test *)
val needle1 = "Alexander"
val needle2 = "Anybody"
val haystack = ["Sasha", "Alex", "Aleksandr", "Alexander", "Al", "Lex"]

val all_except_option_1 = all_except_option(needle1, haystack)
(* answer: SOME ["Lex","Al","Aleksandr","Alex","Sasha"] *)
val all_except_option_2 = all_except_option(needle2, haystack)
(* answer: NONE *)

(* 1b fun get_substitutions1 *)
fun get_substitutions1(substitutions: string list list, word: string) = 
	case substitutions of
		[] => []
		| x::xs => case all_except_option(word, x) of
			NONE => get_substitutions1(xs, word)
			| SOME some_x => some_x @ get_substitutions1(xs, word)
		

(* 1b test *)
val get_substitutions1_1 = get_substitutions1([["Fred","Fredrick"],["Jeff","Jeffrey"],["Geoff","Jeff","Jeffrey"]], "Jeff")
(* answer: ["Jeffrey","Geoff","Jeffrey"] *)

(* 1c fun get_substitution2 *)
fun get_substitutions2(substitutions: string list list, word: string) =
	let
		fun aux(remaining: string list list, word: string, accum: string list) = 
			case remaining of
				[] => accum
				| x::xs => case all_except_option(word, x) of
					NONE => aux(xs, word, accum)
					| SOME some_x => aux(xs, word, some_x @ accum)
	in
		aux(substitutions, word, [])
	end

(* 1c test *)
val get_substitutions2_1 = get_substitutions2([["Fred","Fredrick"],["Jeff","Jeffrey"],["Geoff","Jeff","Jeffrey"]], "Jeff")
(* answer: ["Jeffrey","Geoff","Jeffrey"] *)


(* 1d fun similar_names *)
fun similar_names(substitutions: string list list, full_name) =
	let 
		val {first=first,middle=middle,last=last} = full_name
		val synonyms = get_substitutions2(substitutions, first)
		fun aux(synonyms: string list, middle: string, last: string, accum) =
			case synonyms of
			[] => accum
			| x::xs => aux(xs, middle, last, {first=x, middle=middle, last=last} :: accum)
	in
		full_name :: aux(synonyms, middle, last, [])
	end

(* 1d test *)
val synonyms = [["Fred","Fredrick"],["Elizabeth","Betty"],["Freddie","Fred","F"]]
val similar_names_1 = similar_names(synonyms, {first="Fred", middle="W", last="Smith"})
(* answer: [{first="Fred",last="Smith",middle="W"},
   {first="Fredrick",last="Smith",middle="W"},
   {first="Freddie",last="Smith",middle="W"},
   {first="F",last="Smith",middle="W"}] *)

val similar_name_2 = similar_names(synonyms, {first="Anybody", middle="W", last="Smith"})
(* answer: [{first="Anybody",last="Smith",middle="W"}]*)

(* 2. Solitare game *)

(* 2.a fun card_color *)
fun card_color(card: card) = 
	case card of
		(suit, rank) => case suit of
			Diamonds => Red
			| Hearts => Red
			| Clubs => Black
			| Spades => Black

(* 2.a test *)
val card_ace_clubs = (Clubs, Ace)
val card_ace_hearts = (Hearts, Ace)
val card_jack_spades = (Spades, Jack)
val card_jack_diamonds = (Diamonds, Jack)
val card_3_diamonds = (Diamonds, Num(3))

val card_color_1 = card_color(card_ace_clubs)
val card_color_2 = card_color(card_ace_hearts)
val card_color_3 = card_color(card_jack_spades)
val card_color_4 = card_color(card_jack_diamonds)

(* answers:
val card_color_1 = Black : color
val card_color_2 = Red : color
val card_color_3 = Black : color
val card_color_4 = Red : color
*)

(* 2.b fun card_value *)
fun card_value(card) = 
	case card of
		(suit, rank) => case rank of
			Ace => 11
			| King => 10
			| Queen => 10
			| Jack => 10
			| Num i => i

(* 2.b test *)
val card_value_1 = card_value(card_ace_clubs)
val card_value_2 = card_value(card_ace_hearts)
val card_value_3 = card_value(card_jack_spades)
val card_value_4 = card_value(card_jack_diamonds)
val card_value_5 = card_value(card_3_diamonds)

(*  answer:
val card_value_1 = 11 : int
val card_value_2 = 11 : int
val card_value_3 = 10 : int
val card_value_4 = 10 : int
val card_value_5 = 3 : int
*)


(* 2.c fun remove_card *)
fun remove_card(cs: card list, c: card, exc) =
	let 
		fun aux(needle: card, remaining: card list, accum: card list, found) =
			case remaining of
				[] => if found then accum else raise exc
				| x::xs => if x = needle andalso not found 
					then aux(needle, xs, accum, true) 
					else aux(needle, xs, x :: accum, found)
	in
		aux(c, cs, [], false)
	end

(* 2.c test *)
val cards = [(Clubs, Ace), (Hearts, Jack), (Spades, Queen), (Diamonds, King)]
val remove_card_1 = remove_card(cards, (Diamonds, King), IllegalMove)
(* answer: [(Spades,Queen),(Hearts,Jack),(Clubs,Ace)] *)
(* val remove_card_2 = remove_card(cards, (Diamonds, Ace), IllegalMove)
Exception raises
*)

(* 2.d fun all_same_color *)
fun all_same_color(cs: card list) = 
	case cs of
	[] => true
	| x::xs => case xs of
		[] => true
		| y::ys => if card_color(x) = card_color(y) then all_same_color(xs) else false

(* 2.d test *)
val cards_2 = [(Clubs, Jack), (Clubs, Queen), (Clubs, King), (Clubs, Ace)]
val all_same_color_1 = all_same_color(cards)
val all_same_color_2 = all_same_color(cards_2)
(* answer:
val all_same_color_1 = false : bool
val all_same_color_2 = true : bool
*)

(* 2.e fun sum_cards *)
fun sum_cards(cs: card list) = 
	let 
		fun aux(remaining: card list, accum) = 
			case remaining of
				[] => accum
				| x::xs => aux(xs, card_value(x) + accum)
	in
		aux(cs, 0)
	end

(* 2.e test *)
val cards_3 = [(Clubs, Ace), (Hearts, Ace), (Spades, Ace), (Diamonds, King)]
val sum_cards_1 = sum_cards(cards)
val sum_cards_2 = sum_cards(cards_2)
val sum_cards_3 = sum_cards(cards_3)
(* answer: 
val sum_cards_1 = 41 : int
val sum_cards_2 = 41 : int
val sum_cards_3 = 43 : int
*)

(* 2.f fun score *)
fun score(cs: card list, goal: int) = 
	let
		val card_sum = sum_cards(cs)
		val distance = goal - card_sum
		val preliminary_sum = if distance > 0 then 3 * distance else ~ distance
	in
		if all_same_color(cs)
			then preliminary_sum div 2
			else preliminary_sum
	end


(* 2.f test *)
val score_1 = score(cards_3, 30)
val score_2 = score(cards_3, 48)
(* answer: 
val score_1 = 13 : int
val score_2 = 5 : int
*)

(* 2.g fun officiate *)
fun officiate(cs: card list, moves: move list, goal: int) = 
	let
		fun one_step(held_cards: card list, remaining_cards: card list, remaining_moves: move list, goal: int) =
			case remaining_moves of
				[] => score(held_cards, goal)
				| x::xs => case x of
					Discard the_card => one_step(remove_card(held_cards, the_card, IllegalMove), remaining_cards, xs, goal)
					| Draw => case remaining_cards of
						[] => score(held_cards, goal)
						| y::ys => one_step(y::held_cards, ys, xs, goal)
	in
		one_step([], cs, moves, goal)
	end



(* 2.g test *)
val cs = [(Clubs, Ace), (Hearts, Ace), (Spades, Ace), (Diamonds, King)]
val moves = [Draw, Draw, Draw, Draw]
val moves_2 = [Draw, Draw, Draw, Discard(Clubs, Ace), Draw]
val goal = 35
val officiate_1 = officiate(cs, moves, goal)
val officiate_2 = officiate(cs, moves_2, goal)
val officiate_3 = officiate(cards_2, moves, goal)

(* answer: 
val officiate_1 = 8 : int
val officiate_2 = 3 : int
*)